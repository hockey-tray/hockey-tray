import datetime
import wx.adv
import requests
import sys
import pendulum
import webbrowser
import logging

################
# USER OPTIONS #
################

# Set the following to True to set displayed time to your local timezone,
# it defaults to US/Eastern time when set to False
USE_LOCAL = False

####################
# Global variables #
####################

# hacky way to populate links for each game and fetch when clicked
GAME_LINKS = {}

APP_EXIT = 1
APP_CLOSE = 2
APP_ABOUT = 3
APP_CONF = 4
APP_SCHED = 5

TRAY_TOOLTIP = 'Hockey Tray'
TRAY_LOG = 'hockey-tray.log'
TRAY_VERSION = 'version 1.0'

TZ_OVERRIDE = None

# icons
TRAY_ICON = './icons/icon.png'
HIDE_ICON = './icons/hide.png'
EXIT_ICON = './icons/exit.png'
ABOUT_ICON = './icons/about.png'
CONF_ICON = './icons/settings.png'
SCHED_ICON = './icons/sched.png'


###########
# Logging #
###########
logging.basicConfig(filename=TRAY_LOG, level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S')

def create_menu_item(menu, label, func, link=''):
    item = wx.MenuItem(menu, -1, label, link)
    # drop each unique link into corresponding item id
    GAME_LINKS[item.GetId()] = link
    menu.Bind(wx.EVT_MENU, func, id=item.GetId())
    menu.Append(item)
    return item

class TaskBarIcon(wx.adv.TaskBarIcon):
    def __init__(self, frame):
        self.frame = frame
        super(TaskBarIcon, self).__init__()
        self.set_icon(TRAY_ICON)
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DOWN, self.on_left_down)

    def _convertISOtoTime(self, iso, target='US/Eastern'):
        """Convert the ISO date in UTC time that the API outputs into a
        time (target timezone) formatted with am/pm. Defaults to US/Eastern."""
        date = pendulum.parse(iso).in_tz('{}'.format(target))
        time = date.format('h:mm A z')
        return "{}".format(time)

    def CreatePopupMenu(self, search=None):

        # hack to clear links dictionary each time tray icon is right-clicked
        GAME_LINKS.clear()
        menu = wx.Menu()

        # get date in pacific timezone so day doesn't advance until after all
        # games are final
        if search:
            if 'yesterday' in search:
                date = [pendulum.yesterday().to_date_string()]
            elif 'today' in search:
                # TODO handle 'tomorrow' (middle-click?)
                date = [pendulum.now().in_tz('US/Pacific').to_date_string()]
        else:
            date = [pendulum.yesterday().to_date_string(), 
                    pendulum.tomorrow().to_date_string()]

        content = []
        for day in date:
            # URL for NHL.com API
            url = ("https://statsapi.web.nhl.com/api/v1/schedule?"
                "startDate={}&endDate={}"
                "&expand=schedule.teams,schedule.linescore,"
                "schedule.broadcasts.all,schedule.ticket,"
                "schedule.game.content.media.epg,schedule.radioBroadcasts,"
                "schedule.metadata,schedule.game.seriesSummary,"
                "seriesSummary.series&leaderCategories=&leaderGameTypes=R"
                "&site=en_nhlCA&teamId=&gameType=&timecode=").format(day, day)

            # get data
            content.append(requests.get(url).json())

        global USE_LOCAL, TZ_OVERRIDE
        # timezone handling
        if not TZ_OVERRIDE:
            tz = 'local' if USE_LOCAL else 'US/Eastern'
        else:
            tz = TZ_OVERRIDE

        # parse data
        self._parseGames(menu, content, date, tz)

        menu.AppendSeparator()
        
        # Sched
        m_sched = wx.MenuItem(menu, APP_SCHED, 'Schedule')
        m_sched.SetBitmap(wx.Bitmap(SCHED_ICON))
        menu.Append(m_sched)
        menu.Bind(wx.EVT_MENU, self.on_sched, id=APP_SCHED)

        # Config
        m_config = wx.MenuItem(menu, APP_CONF, 'Settings')
        m_config.SetBitmap(wx.Bitmap(CONF_ICON))
        menu.Append(m_config)
        menu.Bind(wx.EVT_MENU, self.on_config, id=APP_CONF)

        # About
        m_about = wx.MenuItem(menu, APP_ABOUT, 'About')
        m_about.SetBitmap(wx.Bitmap(ABOUT_ICON))
        menu.Append(m_about)
        menu.Bind(wx.EVT_MENU, self.on_about, id=APP_ABOUT)

        # Closes the frame
        m_close = wx.MenuItem(menu, APP_CLOSE, 'Hide')
        m_close.SetBitmap(wx.Bitmap(HIDE_ICON))
        menu.Append(m_close)
        menu.Bind(wx.EVT_MENU, self.on_close, id=APP_CLOSE)

        # Exit menu item w/ custom icon
        m_exit = wx.MenuItem(menu, APP_EXIT, 'Exit')
        m_exit.SetBitmap(wx.Bitmap(EXIT_ICON))
        menu.Append(m_exit)
        menu.Bind(wx.EVT_MENU, self.on_exit, id=APP_EXIT)
        return menu

    def _parseGames(self, menu, games, date, tz='US/Eastern'):
        try:
            if len(games) == 1:
                for game in games[0]['dates'][0]['games']:
                    game_info, game_link = self._parseData(game, tz)
                    create_menu_item(menu, game_info, self.click_summary, game_link)
            else:
                prev_day_menu = wx.Menu()
                next_day_menu = wx.Menu()

                for game in games[0]['dates'][0]['games']:
                    y_game_info, y_game_link = self._parseData(game, tz)
                    # build the menu item
                    create_menu_item(prev_day_menu, y_game_info, self.click_summary, y_game_link)
                for game in games[1]['dates'][0]['games']:
                    n_game_info, n_game_link = self._parseData(game, tz)
                    # build the menu item
                    create_menu_item(next_day_menu, n_game_info, self.click_summary, n_game_link)
                menu.Append(wx.ID_ANY, 'Yesterday', prev_day_menu)
                menu.Append(wx.ID_ANY, 'Tomorrow', next_day_menu)
        except:
            game_info = 'No games found for {}'.format(date)
            gamelink = 'https://www.nhl.com/'
            # build the menu item
            create_menu_item(menu, game_info, self.click_summary, gamelink)

    def _parseData(self, data, tz):
        try:
            status = data['gameType']
            home = data['teams']['home']['team']['abbreviation']
            home_score = data['teams']['home']['score']
            away = data['teams']['away']['team']['abbreviation']
            away_score = data['teams']['away']['score']
            pd_num = data['linescore'].get('currentPeriod')
            period = data['linescore'].get('currentPeriodOrdinal')
            period_rem = data['linescore'].get('currentPeriodTimeRemaining')
            pregame = (True if 'Pre-Game' in data['status']['detailedState'] \
                            else False)
            starting_time = self._convertISOtoTime(data['gameDate'], tz)
            
            # populate the gamecenter URL based on gamePk, default to nhl.com
            try:
                gamelink = 'https://www.nhl.com/gamecenter/{}'.format(
                                                            data['gamePk'])
            except:
                gamelink = 'https://www.nhl.com/'
            
            # convert default values into 'prettier' values
            if period_rem:
                if period_rem == "END":
                    period_rem = "End of"
                if period_rem == "Final":
                    if status == 'P':
                        # post season, no shutouts
                        if pd_num > 3:
                            ot_per = pd_num - 3 if pd_num > 4 else ''
                            period_rem += '/{}OT'.format(ot_per)
                    else:
                        # regular/pre season, we have shutouts :(
                        if pd_num == 4:
                            period_rem += '/OT'
                        elif pd_num > 4:
                            period_rem += '/SO'
                    game_info = ("{} {} {} {} - {}").format(away, 
                                                            away_score, 
                                                            home, home_score, 
                                                            period_rem)
                elif pregame:
                    game_info = ("{} {} {} {} - {}").format(away, 
                                                            away_score, 
                                                            home, home_score, 
                                                            'Pre-Game')
                else:
                    game_info = ("{} {} {} {} - {} {}").format(away, 
                                                            away_score, 
                                                            home, 
                                                            home_score, 
                                                            period_rem, 
                                                            period)
            else:
                game_info = ("{} @ {} - {}").format(away, home, starting_time)
        
            return game_info, gamelink
            ## build the menu item
            #create_menu_item(menu, game_info, self.click_summary, gamelink)
        except:
            game_info = 'No games found for {}'.format(date)
            gamelink = 'https://www.nhl.com/'
            return game_info, gamelink
            ## build the menu item
            #create_menu_item(menu, game_info, self.click_summary, gamelink)
   
    def set_icon(self, path):
        icon = wx.Icon(wx.Bitmap(path))
        self.SetIcon(icon, TRAY_TOOLTIP)

    def click_summary(self, event):
        link = GAME_LINKS[event.GetId()]
        webbrowser.open(link)

    def on_exit(self, event):
        logging.info('Client Exit')
        wx.CallAfter(self.Destroy)
        self.frame.Close()

    def on_close(self, event):
        logging.info('Client Hide')
        self.frame.Hide()

    def on_left_down(self, event):
        menu = self.CreatePopupMenu('today')
        self.PopupMenu(menu)
        menu.Destroy()

    def on_about(self, event):
        licence = """This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>."""

        aboutInfo = wx.adv.AboutDialogInfo()
        aboutInfo.SetName('Hockey Tray')
        aboutInfo.SetIcon(wx.Icon(ABOUT_ICON, wx.BITMAP_TYPE_JPEG))
        aboutInfo.SetVersion(TRAY_VERSION)
        aboutInfo.SetDescription('A NHL System Tray wxPython-based application that'
                                 ' retrieves scores and other information.')
        aboutInfo.AddDeveloper('cottongin')
        aboutInfo.AddDeveloper('grateful')
        aboutInfo.SetLicence(licence)
        logging.info('About Clicked')
        wx.adv.AboutBox(aboutInfo, self.frame)

    def on_config(self, event):
        Settings(None).Show()

    def on_sched(self, event):
        Sched(None).Show()

class App(wx.App):
    def OnInit(self):
        pendulum.set_formatter('alternative')
        frame=wx.Frame(None)
        self.SetTopWindow(frame)
        TaskBarIcon(frame)
        return True

class Settings(wx.Frame):

    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds, title="Settings", pos=wx.DefaultPosition,
                          style=wx.CLOSE_BOX, size=(400,350))
        pnl = wx.Panel(self, wx.ID_ANY)
        szmain = wx.BoxSizer(wx.VERTICAL)
        now = pendulum.now().timezone_name
        timezone_choices = (now,) + pendulum.timezones
        self.timezone_options = wx.ListBox(pnl, wx.ID_ANY, style=wx.LB_SINGLE,
                                           choices=timezone_choices)
        self.timezone_options.SetSelection(0)
        txt = wx.StaticText(pnl, label="Select Time Zone")
        self.select_button = wx.Button(pnl, wx.ID_ANY, "Save")
        szmain.Add(txt, 0, wx.ALL|wx.EXPAND, 10)
        szmain.Add(self.timezone_options, 0, wx.ALL|wx.EXPAND, 10)
        szmain.Add(self.select_button, 0, wx.ALL|wx.EXPAND, 10)
        pnl.SetSizer(szmain)

        self.select_button.Bind(wx.EVT_BUTTON, self.OnSelectButton)

    def OnSelectButton(self, event):
        global USE_LOCAL, TZ_OVERRIDE
        TZ_OVERRIDE = self.timezone_options.GetString(self.timezone_options.GetSelection())
        USE_LOCAL = False
        self.Close()

class Sched(wx.Frame):

    def _convertISOtoTime(self, iso, target='US/Eastern'):
        """Convert the ISO date in UTC time that the API outputs into a
        time (target timezone) formatted with am/pm. Defaults to US/Eastern."""
        date = pendulum.parse(iso).in_tz('{}'.format(target))
        time = date.format('h:mm A z')
        return "{}".format(time)


    def __init__(self, *args, **kwds):
        wx.Frame.__init__(self, *args, **kwds, title="Schedule", pos=wx.DefaultPosition,
                          style=wx.CLOSE_BOX, size=(200,400))
        self.pnl = wx.Panel(self, wx.ID_ANY)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)
        self.result = wx.StaticText(self, label="")  
        self.result.Wrap(200)
        self.dpc = wx.adv.DatePickerCtrl(self, style=wx.adv.DP_DROPDOWN)
        self.Bind(wx.adv.EVT_DATE_CHANGED, self.OnDateChanged, self.dpc)
        self.sizer.Add(self.dpc, 0, wx.EXPAND, 1)
        self.sizer.AddSpacer(5)
        self.sizer.Add(self.result, 0, wx.EXPAND, 1)

    def OnDateChanged(self, event):
        try:
            date = datetime.datetime.strptime(str(event.GetDate()), "%a %b %d %H:%M:%S %Y").strftime("%Y-%m-%d")
            self.SendGames(date)
        except:
            return

    def SendGames(self, date):
        url = ("https://statsapi.web.nhl.com/api/v1/schedule?"
               "startDate={}&endDate={}"
               "&expand=schedule.teams,schedule.linescore,"
               "schedule.broadcasts.all,schedule.ticket,"
               "schedule.game.content.media.epg,schedule.radioBroadcasts,"
               "schedule.metadata,schedule.game.seriesSummary,"
               "seriesSummary.series&leaderCategories=&leaderGameTypes=R"
               "&site=en_nhlCA&teamId=&gameType=&timecode=").format(date, date)
        game_info = []
        content = requests.get(url).json()
        game_info.append("{}\n".format(date))
        game_info.append("-------------------------------------------------------------------------------\n")
        try:
            for data in content['dates'][0]['games']:
                status = data['gameType']
                home = data['teams']['home']['team']['abbreviation']
                home_score = data['teams']['home']['score']
                away = data['teams']['away']['team']['abbreviation']
                away_score = data['teams']['away']['score']
                pd_num = data['linescore'].get('currentPeriod')
                period = data['linescore'].get('currentPeriodOrdinal')
                period_rem = data['linescore'].get('currentPeriodTimeRemaining')
                pregame = (True if 'Pre-Game' in data['status']['detailedState'] \
                                else False)
                starting_time = self._convertISOtoTime(data['gameDate'], 'US/Eastern')

                if period_rem:
                    if period_rem == "END":
                        period_rem = "End of"
                    if period_rem == "Final":
                        if status == 'P':
                            # post season, no shutouts
                            if pd_num > 3:
                                ot_per = pd_num - 3 if pd_num > 4 else ''
                                period_rem += '/{}OT'.format(ot_per)
                        else:
                            # regular/pre season, we have shutouts :(
                            if pd_num == 4:
                                period_rem += '/OT'
                            elif pd_num > 4:
                                period_rem += '/SO'
                        game_info.append("{} {} {} {} - {}\n".format(away, 
                                                                away_score, 
                                                                home, home_score, 
                                                                period_rem))
                    elif pregame:
                        game_info.append("{} {} {} {} - {}\n".format(away, 
                                                                away_score, 
                                                                home, home_score, 
                                                                'Pre-Game'))
                    else:
                        game_info.append("{} {} {} {} - {} {}\n".format(away, 
                                                                away_score, 
                                                                home, 
                                                                home_score, 
                                                                period_rem, 
                                                                period))
                else:
                    game_info.append("{} @ {} - {}\n".format(away, home, starting_time))
        except:
            game_info.append("No games found.")
        game_info = ''.join(game_info)
        self.result.SetLabel(game_info)


def main():
    app = App(False)
    logging.info('Started')
    app.MainLoop()

if __name__ == '__main__':
    main()
